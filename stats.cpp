#include <iostream>

int main(){
  float  num1, num2;
  std::cout << "Enter 2 numbers: ";
  std::cin>>num1>>num2;

  std::cout <<"Sum : "<<num1+num2<<std::endl;
  std::cout <<"Difference : "<<num1-num2<<std::endl;
  std::cout <<"Product : "<<num1*num2<<std::endl;
  std::cout <<"Distance : "<<abs(num1-num2)<<std::endl;
  std::cout <<"Mean : "<<(num1+num2)/2.0<<std::endl;
  return 0;
}
